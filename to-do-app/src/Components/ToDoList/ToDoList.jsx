import { useState, useEffect } from "react";
import ToDo from "../ToDo/ToDo";
import "./ToDoList.css";

export default function ToDoList({ setAddToDoRef }) {
  const [dataLoaded, setDataLoaded] = useState(false);
  const [toDoList, setToDoList] = useState([]);

  useEffect(() => {
    if (!dataLoaded) {
      setDataLoaded(true);
      setAddToDoRef(addToDo);
      const storageToDoList = localStorage.getItem("to-do-list");
      console.log(storageToDoList);
      if (storageToDoList) {
        const parsedJSON = JSON.parse(storageToDoList);
        setToDoList(parsedJSON.data);
      }
    }
  }, [dataLoaded, setAddToDoRef]);

  useEffect(() => {
    localStorage.setItem("to-do-list", JSON.stringify({ data: toDoList }));
  }, [toDoList]);

  function addToDo(toDo) {
    setToDoList((prevState) => [...prevState, { task: toDo, complete: false }]);
  }

  function deleteToDo(delIndex) {
    setToDoList((prevState) => [
      ...prevState.filter((element, index) => index !== delIndex),
    ]);
  }

  function toggleToDoStatus(toggleIndex) {
    setToDoList((prevState) => [
      ...prevState.map((element, index) =>
        index === toggleIndex
          ? { ...element, complete: !element.complete }
          : element
      ),
    ]);
  }

  function changeToDo(changeIndex, toDo) {
    setToDoList((prevState) => [
      ...prevState.map((element, index) =>
        index === changeIndex ? { ...element, task: toDo } : element
      ),
    ]);
  }

  return (
    <div className="to-do-list">
      {toDoList.map((data, index) => (
        <ToDo
          key={index}
          index={index}
          data={data}
          allfunctions={{
            toggleToDoStatus: toggleToDoStatus,
            deleteToDo: deleteToDo,
            changeToDo: changeToDo,
          }}
        />
      ))}
    </div>
  );
}
