import { useState } from "react";
import { PlusIcon } from "../SVGs/Icons";
import "./AddButton.css";

export default function AddButton({ addToDoRef }) {
  const [inputToDo, setInputToDo] = useState("");
  const [showInput, setShowInput] = useState(false);

  function handleClick() {
    if (!showInput) {
      setShowInput(true);
    } else {
      if (inputToDo) {
        addToDoRef.current(inputToDo);
        setInputToDo("");
        setShowInput(false);
      }
    }
  }

  return (
    <div className="add-to-do-wrapper">
      {showInput ? (
        <div className="add-field">
          <input
            className="add-input"
            value={inputToDo}
            type="text"
            onChange={(e) => setInputToDo(e.target.value)}
            placeholder="Add To-Do"
          />
        </div>
      ) : null}
      <div className="add-button-wrapper">
        <button className="add-button" type="button" onClick={handleClick}>
          <PlusIcon />
        </button>
      </div>
    </div>
  );
}
