import { useState } from "react/cjs/react.development";
import { CheckedBoxIcon, DeleteIcon, UncheckedBoxIcon } from "../SVGs/Icons";
import "./ToDo.css";

export default function ToDo({ index, data, allfunctions }) {
  const { toggleToDoStatus, deleteToDo, changeToDo } = allfunctions;

  const [edit, setEdit] = useState(false);
  const [task, setTask] = useState(data.task);

  function handleEdit() {
    if (task) {
      changeToDo(index, task);
      setEdit(false);
    }
  }

  return (
    <div className="to-do-item">
      <div className="done-button-wrapper">
        <button
          type="button"
          className="done-button"
          onClick={() => toggleToDoStatus(index)}
        >
          {data.complete ? <CheckedBoxIcon /> : <UncheckedBoxIcon />}
        </button>
      </div>
      <div className="to-do-edit">
        {edit ? (
          <div className="edit-task">
            <input
              className="task-input"
              type="text"
              value={task}
              placeholder="Add Task"
              onChange={(e) => setTask(e.target.value)}
            />
            <button className="edit-button" type="button" onClick={handleEdit}>
              Set
            </button>
          </div>
        ) : (
          <button
            className="edit-task-button"
            type="button"
            onClick={() => setEdit(true)}
          >
            <span className="to-do">{data.task}</span>
          </button>
        )}
      </div>
      <div className="delete-button-wrapper">
        <button
          type="button"
          className="delete-button"
          onClick={() => deleteToDo(index)}
        >
          <DeleteIcon />
        </button>
      </div>
    </div>
  );
}
