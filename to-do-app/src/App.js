import { useRef } from "react";
import "./App.css";
import AddButton from "./Components/AddButton/AddButton";
import ToDoList from "./Components/ToDoList/ToDoList";

const Header = () => (
  <header className="app-header">
    <h1>To-Do App</h1>
  </header>
);

function App() {
  const addToDoRef = useRef(null);

  function setAddToDoRef(func) {
    addToDoRef.current = func;
  }

  return (
    <div className="App">
      <Header />
      <section className="to-do-section">
        <ToDoList setAddToDoRef={setAddToDoRef} />
        <AddButton addToDoRef={addToDoRef} />
      </section>
    </div>
  );
}

export default App;
