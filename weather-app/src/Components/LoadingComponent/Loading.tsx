import { motion, Variants } from 'framer-motion';

const loaderVariants: Variants = {
  animation: {
    y: [0, -60],
    transition: {
      y: {
        repeatType: 'reverse',
        repeat: Infinity,
        duration: 0.5,
        ease: 'easeOut',
      },
    },
  },
};

export default function Loader(): JSX.Element {
  return (
    <div className="loader-wrapper">
      <motion.div
        className="loader"
        variants={loaderVariants}
        animate="animation"
      />
    </div>
  );
}
