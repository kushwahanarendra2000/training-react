import { useState } from 'react';
import { weekWeatherType } from '../../Services/services.types';
import DayForecast from '../DayForecast/DayForecast';
import './NextDays.css';

interface NextDaysProps {
  weekWeather: weekWeatherType;
}

export default function NextDays({ weekWeather }: NextDaysProps): JSX.Element {
  const [expanded, setExpanded] = useState<false | number>(false);

  return (
    <div className="next-days">
      <h1 className="details-head pad-bottom">Daily Weather</h1>
      <div className="day-forecast-list">
        {weekWeather.map((element, index) => (
          <DayForecast
            key={element.day}
            index={index}
            expanded={expanded}
            setExpanded={setExpanded}
            data={element}
          />
        ))}
      </div>
    </div>
  );
}
