import { currentWeatherInterface } from '../../Services/services.types';

interface CurrentForecastProps {
  currentWeather: currentWeatherInterface;
}

export default function CurrentForecast({
  currentWeather,
}: CurrentForecastProps): JSX.Element {
  return (
    <div className="weather-details">
      <h4 className="details-head">Current Weather Details</h4>
      <div className="details">
        <div className="row">
          <h5 className="key">Feels Like</h5>
          <h5 className="value">{currentWeather.feelsLike}</h5>
        </div>
        <div className="row">
          <h5 className="key">Sunrise</h5>
          <h5 className="value">{currentWeather.sunrise}</h5>
        </div>
        <div className="row">
          <h5 className="key">Sunset</h5>
          <h5 className="value">{currentWeather.sunset}</h5>
        </div>
        <div className="row">
          <h5 className="key">Humidity</h5>
          <h5 className="value">{currentWeather.humidity}</h5>
        </div>
        <div className="row">
          <h5 className="key">Cloudiness</h5>
          <h5 className="value">{currentWeather.cloudiness}</h5>
        </div>
        <div className="row">
          <h5 className="key">Atmospheric Pressure</h5>
          <h5 className="value">{currentWeather.atmosphericPressure}</h5>
        </div>
        <div className="row">
          <h5 className="key">Wind Speed</h5>
          <h5 className="value">{currentWeather.windSpeed}</h5>
        </div>
        <div className="row">
          <h5 className="key">Average Visibility</h5>
          <h5 className="value">{currentWeather.averageVisibility}</h5>
        </div>
      </div>
    </div>
  );
}
