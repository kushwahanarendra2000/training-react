import './Forecast.css';

import NextDays from '../NextDays/NextDays';
import CurrentForecast from '../CurrentForecast/CurrentForecast';
import SearchBox from '../SearchBox/SearchBox';
import {
  currentWeatherInterface,
  weekWeatherType,
} from '../../Services/services.types';

type timezone = { timezone: string; offset: number | null };
type coordinates = { lat: number; lon: number };

interface TimeZoneProps {
  timezone: timezone;
  coordinates: coordinates;
}

function TimeZone({ timezone, coordinates }: TimeZoneProps): JSX.Element {
  return (
    <div className="time-zone">
      <h3 className="timezone">{timezone.timezone}</h3>
      <h5 className="latlon">{`${coordinates.lat}N ${coordinates.lon}E`}</h5>
    </div>
  );
}

interface ForecastProps {
  handleCoordinateChange: (pos: { lat: number; lon: number }) => void;
  currentWeather: currentWeatherInterface;
  weekWeather: weekWeatherType;
  timezone: timezone;
  coordinates: coordinates;
  showNotification: (text: string) => void;
  showLoader: () => void;
  hideLoader: () => void;
}

export default function Forecast({
  currentWeather,
  weekWeather,
  timezone,
  coordinates,
  handleCoordinateChange,
  showNotification,
  showLoader,
  hideLoader,
}: ForecastProps): JSX.Element {
  return (
    <div className="scroll">
      <TimeZone timezone={timezone} coordinates={coordinates} />
      <SearchBox
        handleCoordinateChange={handleCoordinateChange}
        showNotification={showNotification}
        showLoader={showLoader}
        hideLoader={hideLoader}
      />
      <CurrentForecast currentWeather={currentWeather} />
      <NextDays weekWeather={weekWeather} />
    </div>
  );
}
