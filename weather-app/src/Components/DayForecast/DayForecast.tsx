import { Dispatch, SetStateAction } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import './DayForecast.css';
import { weekWeatherObjType } from '../../Services/services.types';

interface DayForecastHeaderProps {
  isOpen: boolean;
  min: string;
  max: string;
  day: string;
  img: string;
}

function DayForecastHeader({
  isOpen,
  min,
  max,
  img,
  day,
}: DayForecastHeaderProps): JSX.Element {
  return (
    <div className="day-forecast-wrapper">
      <div className={`day-forecast ${isOpen ? 'clicked-day' : ''}`}>
        <div className="week-day">
          <h4 className="forecast-details">{day}</h4>
        </div>
        <div className="forecast-temp">
          <h4 className="temp-value">{max}</h4>
          <h4 className="temp-value less-opaque">{min}</h4>
        </div>
        <div className="forecast-icon">
          <img
            alt={img}
            src={`http://openweathermap.org/img/wn/${img}@2x.png`}
          />
        </div>
      </div>
    </div>
  );
}

interface DayForecastContentProps {
  data: weekWeatherObjType;
}

function DayForecastContent({ data }: DayForecastContentProps): JSX.Element {
  return (
    <div className="next-day-details">
      <div className="row">
        <h5 className="key">Night</h5>
        <h5 className="value">{data.nightTemp}</h5>
      </div>
      <div className="row">
        <h5 className="key">Day</h5>
        <h5 className="value">{data.dayTemp}</h5>
      </div>
      <div className="row">
        <h5 className="key">Sunrise</h5>
        <h5 className="value">{data.sunrise}</h5>
      </div>
      <div className="row">
        <h5 className="key">Sunset</h5>
        <h5 className="value">{data.sunset}</h5>
      </div>
      <div className="row">
        <h5 className="key">Humidity</h5>
        <h5 className="value">{data.humidity}</h5>
      </div>
      <div className="row">
        <h5 className="key">Cloudiness</h5>
        <h5 className="value">{data.cloudiness}</h5>
      </div>
      <div className="row">
        <h5 className="key">Atmospheric Pressure</h5>
        <h5 className="value">{data.atmosphericPressure}</h5>
      </div>
      <div className="row">
        <h5 className="key">Wind Speed</h5>
        <h5 className="value">{data.windSpeed}</h5>
      </div>
    </div>
  );
}

interface DayForecastProps {
  index: number;
  setExpanded: Dispatch<SetStateAction<false | number>>;
  expanded: false | number;
  data: weekWeatherObjType;
}

export default function DayForecast({
  index,
  data,
  expanded,
  setExpanded,
}: DayForecastProps): JSX.Element {
  const isOpen = index === expanded;
  return (
    <>
      <motion.div
        className="forecast-header-wrapper"
        initial={false}
        onClick={() => setExpanded(isOpen ? false : index)}
      >
        <DayForecastHeader
          isOpen={isOpen}
          min={data.min}
          max={data.max}
          img={data.img}
          day={data.day}
        />
      </motion.div>
      <AnimatePresence initial={false}>
        {isOpen && (
          <motion.div
            key="content"
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: { opacity: 1, height: 'auto' },
              collapsed: { opacity: 0, height: 0 },
            }}
            transition={{ duration: 0.8, ease: [0.04, 0.62, 0.23, 0.98] }}
          >
            <DayForecastContent data={data} />
          </motion.div>
        )}
      </AnimatePresence>
    </>
  );
}
