import { motion, AnimatePresence } from 'framer-motion';

const notificationVariants = {
  initial: {
    opacity: 0,
    y: 50,
    scale: 0.2,
    transition: { duration: 0.1 },
  },
  animate: {
    opacity: 1,
    y: 0,
    scale: 1,
  },
  exit: {
    opacity: 0,
    scale: 0.2,
    transition: { ease: 'easeOut', duration: 0.15 },
  },
  hover: { scale: 1.05, transition: { duration: 0.1 } },
};

interface NotificationProps {
  text: string;
  handleClose: () => void;
}

function Notification({ text, handleClose }: NotificationProps): JSX.Element {
  return (
    <>
      <h3 className="notification-text">{text}</h3>
      <motion.div
        className="close"
        whileHover={{ scale: 1.2 }}
        onClick={handleClose}
      >
        <svg width="18" height="18" viewBox="0 0 23 23">
          <motion.path
            fill="transparent"
            strokeWidth="3"
            stroke="#ffffff"
            strokeLinecap="square"
            d="M 3 16.5 L 17 2.5"
          />
          <motion.path
            fill="transparent"
            strokeWidth="3"
            stroke="#ffffff"
            strokeLinecap="square"
            d="M 3 2.5 L 17 16.346"
          />
        </svg>
      </motion.div>
    </>
  );
}

interface NotificationContainerProps {
  text: string;
  visible: boolean;
  handleClose: () => void;
}

export default function NotificationContainer({
  text,
  visible,
  handleClose,
}: NotificationContainerProps): JSX.Element {
  return (
    <div className="notification">
      <AnimatePresence initial={false}>
        {visible && (
          <motion.div
            className="notification-wrapper"
            layout
            variants={notificationVariants}
            whileHover="hover"
            initial="initial"
            animate="animate"
            exit="exit"
          >
            <Notification text={text} handleClose={handleClose} />
          </motion.div>
        )}
      </AnimatePresence>
    </div>
  );
}
