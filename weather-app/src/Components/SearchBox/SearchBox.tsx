import { useState } from 'react';
import SearchIcon from '../../Assets/searchIcon.png';
import { getCoordinatesFromAPI } from '../../Services/services';

interface SearchBoxProps {
  handleCoordinateChange: (pos: { lat: number; lon: number }) => void;
  showNotification: (text: string) => void;
  showLoader: () => void;
  hideLoader: () => void;
}

export default function SearchBox({
  handleCoordinateChange,
  showNotification,
  showLoader,
  hideLoader,
}: SearchBoxProps): JSX.Element {
  const [searchValue, setSearchValue] = useState<string>('');

  const handleSearch = async () => {
    showLoader();
    if (searchValue) {
      const numberOfCommas = (searchValue.match(/,/g) || []).length;
      if (numberOfCommas === 1) {
        // parsing float coordinates from input
        const [lat, lon] = searchValue.split(',').map(parseFloat);

        if (lat > -90 && lat < 90 && lon > -180 && lon < 180) {
          hideLoader();
          handleCoordinateChange({ lat, lon });
          return;
        }
        if (!Number.isNaN(lat) || !Number.isNaN(lon)) {
          // if inputs are numbers don't fall back
          hideLoader();
          showNotification('Wrong Input Coordinates');
          return;
        }
      }

      // fallback to name

      const coordinates = await getCoordinatesFromAPI(searchValue);
      if ('error' in coordinates) {
        hideLoader();
        showNotification(`${coordinates.error} or wrong input provided.`);
        return;
      }
      hideLoader();
      handleCoordinateChange(coordinates);
    }
  };

  return (
    <div className="input-coor">
      <input
        className="input-box"
        type="text"
        placeholder="Another Location Name (or Lat, lon)"
        onChange={(e) => {
          setSearchValue(e.target.value);
        }}
      />
      <button type="button" className="search-button" onClick={handleSearch}>
        <img className="search-icon" alt="search" src={SearchIcon} />
      </button>
    </div>
  );
}
