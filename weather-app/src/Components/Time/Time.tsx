import moment, { Moment } from 'moment';
import { useEffect, useState } from 'react';
import { getDateFromMoment } from '../../Services/services';
import './Time.css';

interface TimeProps {
  location: string;
  timezone: { timezone: string; offset: null | number };
  temperatureAndIcon: { temp: number | string; icon: string };
}

export default function Time({
  location,
  timezone,
  temperatureAndIcon,
}: TimeProps): JSX.Element {
  const [time, setTime] = useState<Moment | null>(null);

  useEffect(() => {
    const interval = setInterval(() => {
      if (timezone.timezone && timezone.offset !== null) {
        setTime(moment().utcOffset(timezone.offset / 60));
      }
    }, 1000);
    return () => clearInterval(interval);
  }, [timezone]);

  const { temp, icon } = temperatureAndIcon;

  return (
    <div className="time-and-zone">
      {time ? (
        <div className="time-and-date">
          <div>
            <h1 className="big-time">{time.format('hh:mm')}</h1>
            <h5 className="time-period">{time.format('A')}</h5>
          </div>
          <h3 className="date">{getDateFromMoment(time)}</h3>
        </div>
      ) : (
        <div className="time-and-date">
          <div>
            <h1 className="big-time">HH:MM</h1>
            <h5 className="time-period">AM/PM</h5>
          </div>
          <h3 className="date">Day, Date Month</h3>
        </div>
      )}
      <div className="temp-and-city">
        <div className="temp-city">
          <div className="temp-degree">
            <h1 className="temp">{temp}</h1>
            <h2 className="degree">&#176;</h2>
          </div>
          <h1 className="city">{location}</h1>
        </div>
        <div className="weather-icon">
          {icon ? (
            <img
              alt="weather"
              className="iconImg"
              src={`http://openweathermap.org/img/wn/${icon}@2x.png`}
            />
          ) : null}
        </div>
      </div>
    </div>
  );
}
