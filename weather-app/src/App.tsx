import { useCallback, useEffect, useRef, useState } from 'react';
import Forecast from './Components/Forecast/Forecast';
import Time from './Components/Time/Time';
import {
  INIT_WEATHER,
  getLocationFromAPI,
  getWeatherFromAPI,
} from './Services/services';
import {
  currentWeatherInterface,
  weekWeatherType,
} from './Services/services.types';
import './App.css';
import NotificationContainer from './Components/Notification/Notification';
import Loader from './Components/LoadingComponent/Loading';

function App(): JSX.Element {
  const [location, setLocation] = useState('....');
  const [timezone, setTimezone] = useState<{
    timezone: string;
    offset: null | number;
  }>({ timezone: '', offset: null });

  const [coordinates, setCoordinates] = useState({ lat: 0, lon: 0 });
  const [temperatureAndIcon, setTemperatureAndIcon] = useState<{
    temp: number | string;
    icon: string;
  }>({ temp: '-', icon: '' });

  const [notificationText, setNotificationText] = useState('');
  const [notificationVisible, setNotificationVisible] = useState(false);
  const notificationTimeOut = useRef<ReturnType<typeof setTimeout> | null>(
    null,
  ); // hide notification timeout

  const [loaderVisible, setLoaderVisible] = useState(false);

  const [currentWeather, setCurrentWeather] =
    useState<currentWeatherInterface>(INIT_WEATHER);
  const [weekWeather, setWeekWeather] = useState<weekWeatherType>([]);

  // states end

  const showLoader = () => {
    setLoaderVisible(true);
  };

  const hideLoader = () => {
    setLoaderVisible(false);
  };

  const hideNotification = () => {
    setNotificationVisible(false);
    if (notificationTimeOut.current) {
      clearTimeout(notificationTimeOut.current);
    }
  };

  const showNotification = (text: string) => {
    setNotificationText(text);
    setNotificationVisible(true);
    notificationTimeOut.current = setTimeout(hideNotification, 5000);
  };

  const handleCoordinateChange: (pos: { lat: number; lon: number }) => void =
    useCallback(async (pos: { lat: number; lon: number }) => {
      showLoader();
      const LOCATION = await getLocationFromAPI(pos);
      if (!(typeof LOCATION === 'string')) {
        showNotification(LOCATION.error);
        return;
      }

      const WEATHER = await getWeatherFromAPI(pos);
      if ('error' in WEATHER) {
        showNotification(WEATHER.error);
        return;
      }

      setCoordinates({
        lat: pos.lat,
        lon: pos.lon,
      });

      setLocation(LOCATION);

      setTimezone({ timezone: WEATHER.timezone, offset: WEATHER.offset });
      setTemperatureAndIcon({
        temp: WEATHER.temp,
        icon: WEATHER.icon,
      });

      setCurrentWeather(WEATHER.currentWeather);
      setWeekWeather(WEATHER.weekWeather);
      hideLoader();
    }, []);

  useEffect(() => {
    if (navigator.geolocation) {
      showLoader();
      navigator.geolocation.getCurrentPosition(
        (pos) => {
          hideLoader();
          handleCoordinateChange({
            lat: pos.coords.latitude,
            lon: pos.coords.longitude,
          });
        },
        () => {
          hideLoader();
          showNotification(
            'Location not provided. Search by location name or by coordinates.',
          );
        },
      );
    }
  }, []);

  useEffect(() => {
    const interval = setInterval(async () => {
      if (coordinates.lat || coordinates.lon) {
        const WEATHER = await getWeatherFromAPI(coordinates);
        if ('error' in WEATHER) {
          showNotification(WEATHER.error);
          return;
        }

        setTemperatureAndIcon({
          temp: WEATHER.temp,
          icon: WEATHER.icon,
        });

        setCurrentWeather(WEATHER.currentWeather);
        setWeekWeather(WEATHER.weekWeather);
      }
    }, 60000);
    return () => clearInterval(interval);
  }, []);

  return (
    <div className="App">
      <div className="left">
        <Time
          location={location}
          timezone={timezone}
          temperatureAndIcon={temperatureAndIcon}
        />
      </div>
      <div className="right">
        <Forecast
          handleCoordinateChange={handleCoordinateChange}
          currentWeather={currentWeather}
          weekWeather={weekWeather}
          timezone={timezone}
          coordinates={coordinates}
          showNotification={showNotification}
          showLoader={showLoader}
          hideLoader={hideLoader}
        />
      </div>
      <NotificationContainer
        visible={notificationVisible}
        text={notificationText}
        handleClose={hideNotification}
      />
      {loaderVisible && <Loader />}
    </div>
  );
}

export default App;
