/* eslint-disable camelcase */
export interface currentWeatherInterface {
  feelsLike: string;
  sunrise: string;
  sunset: string;
  humidity: string;
  cloudiness: string;
  atmosphericPressure: string;
  windSpeed: string;
  averageVisibility: string;
}

export type getLocationFromAPIType = (pos: {
  lat: number;
  lon: number;
}) => Promise<string | { error: string }>;

export type weekWeatherObjType = {
  day: string;
  min: string;
  max: string;
  dayTemp: string;
  nightTemp: string;
  img: string;
  sunrise: string;
  sunset: string;
  humidity: string;
  cloudiness: string;
  atmosphericPressure: string;
  windSpeed: string;
};

export type weekWeatherType = weekWeatherObjType[];

export type getWeatherFromAPIType = (pos: {
  lat: number;
  lon: number;
}) => Promise<
  | {
      timezone: string;
      offset: number;
      temp: number;
      icon: string;
      currentWeather: currentWeatherInterface;
      weekWeather: weekWeatherType;
    }
  | { error: string }
>;

export type getCoordinatesFromAPIType = (
  location: string,
) => Promise<{ error: string } | { lat: number; lon: number }>;

export interface apiDataType {
  timezone: string;
  timezone_offset: number;
  current: {
    dt: number;
    sunrise: number;
    sunset: number;
    temp: number;
    feels_like: number;
    pressure: number;
    humidity: number;
    clouds: number;
    visibility: number;
    wind_speed: number;
    weather: { icon: string }[];
  };
  daily: {
    dt: number;
    sunrise: number;
    sunset: number;
    temp: { day: number; min: number; max: number; night: number };
    pressure: number;
    humidity: number;
    wind_speed: number;
    weather: { icon: string }[];
    clouds: number;
  }[];
}
