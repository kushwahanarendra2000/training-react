import moment, { Moment } from 'moment';
import axios from 'axios';
import {
  currentWeatherInterface,
  getLocationFromAPIType,
  weekWeatherType,
  getWeatherFromAPIType,
  apiDataType,
  getCoordinatesFromAPIType,
} from './services.types';

export function getDateFromMoment(momentTime: Moment): string {
  return `${momentTime.format('dddd')}, ${momentTime.format(
    'D',
  )} ${momentTime.format('MMM')}`;
}

export const INIT_WEATHER: currentWeatherInterface = {
  feelsLike: '....',
  sunrise: '....',
  sunset: '....',
  humidity: '....',
  cloudiness: '....',
  atmosphericPressure: '....',
  windSpeed: '....',
  averageVisibility: '....',
};

export const getLocationFromAPI: getLocationFromAPIType = async (pos) => {
  try {
    const response = await axios.get(
      `${process.env.REACT_APP_LOCATION_API}/reverse`,
      { params: { lat: pos.lat, lon: pos.lon, format: 'json' } },
    );

    if (response.data?.error) {
      throw new Error(response.data.error);
    }

    if (response.data.address?.city) {
      return response.data.address.city;
    }
    if (response.data.address?.state_district) {
      return response.data.address.state_district;
    }
    if (response.data.address?.county) {
      return response.data.address?.county;
    }
    if (response.data.address?.state) {
      return response.data.address?.state;
    }
    return response.data.country;
  } catch (error) {
    return { error: 'Error fetching location.' };
  }
};

function getCurrentWeather(apiData: apiDataType): currentWeatherInterface {
  return {
    feelsLike: `${Math.round(apiData.current.feels_like)} \u00B0`,
    sunrise: `${moment(apiData.current.sunrise * 1000)
      .utcOffset(apiData.timezone_offset / 60)
      .format('hh:mm A')}`,
    sunset: `${moment(apiData.current.sunset * 1000)
      .utcOffset(apiData.timezone_offset / 60)
      .format('hh:mm A')}`,
    humidity: `${apiData.current.humidity}%`,
    cloudiness: `${apiData.current.clouds}%`,
    atmosphericPressure: `${apiData.current.pressure} hPa`,
    windSpeed: `${apiData.current.wind_speed} m/s`,
    averageVisibility: `${apiData.current.visibility} m`,
  };
}

function getWeekWeather(apiData: apiDataType): weekWeatherType {
  const offset = apiData.timezone_offset;
  const week: weekWeatherType = [];

  apiData.daily.forEach((element, index: number) => {
    let day = '';

    if (index === 0) {
      day = 'Today';
    } else if (index === 1) {
      day = 'Tomorrow';
    } else {
      day = `${moment(element.dt * 1000)
        .utcOffset(offset / 60)
        .format('dddd')}`;
    }

    week.push({
      day,
      dayTemp: `${Math.round(element.temp.day)}\u00B0`,
      nightTemp: `${Math.round(element.temp.night)}\u00B0`,
      min: `${Math.round(element.temp.min)}\u00B0`,
      max: `${Math.round(element.temp.max)}\u00B0`,
      img: element.weather[0].icon,
      sunrise: `${moment(element.sunrise * 1000)
        .utcOffset(offset / 60)
        .format('hh:mm A')}`,
      sunset: `${moment(element.sunset * 1000)
        .utcOffset(offset / 60)
        .format('hh:mm A')}`,
      humidity: `${element.humidity}%`,
      cloudiness: `${element.clouds}%`,
      atmosphericPressure: `${element.pressure} hPa`,
      windSpeed: `${element.wind_speed} m/s`,
    });
  });

  return week;
}

export const getWeatherFromAPI: getWeatherFromAPIType = async (pos) => {
  try {
    const response = await axios.get(
      `${process.env.REACT_APP_WEATHER_API}/data/2.5/onecall`,
      {
        params: {
          lat: pos.lat,
          lon: pos.lon,
          exclude: 'hourly,minutely',
          units: 'metric',
          appid: process.env.REACT_APP_API_KEY,
        },
      },
    );

    return {
      timezone: response.data.timezone,
      offset: response.data.timezone_offset,
      temp: Math.round(response.data.current.temp),
      icon: response.data.current.weather[0].icon,
      currentWeather: getCurrentWeather(response.data),
      weekWeather: getWeekWeather(response.data),
    };
  } catch (error) {
    return { error: 'Error getting weather' };
  }
};

export const getCoordinatesFromAPI: getCoordinatesFromAPIType = async (
  location,
) => {
  try {
    const response = await axios.get(`${process.env.REACT_APP_LOCATION_API}`, {
      params: {
        addressdetails: 1,
        q: location,
        format: 'json',
        limit: 1,
      },
    });
    if (!response.data.length) {
      throw new Error('Error getting coordinates');
    }
    return { lat: response.data[0].lat, lon: response.data[0].lon };
  } catch (error) {
    return { error: 'Error getting coordinates' };
  }
};
