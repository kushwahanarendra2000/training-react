import './App.css';
import RegistrationForm from './Components/RegistrationForm/Registration';

export default function App() {
  return (
    <div className="App">
      <RegistrationForm message="Registration" />
    </div>
  );
}
