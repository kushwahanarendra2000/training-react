import * as Yup from 'yup';
import { withFormik, FormikProps, Form, Field, useField } from 'formik';
import { ReactNode } from 'react';
import './Registration.css';

interface CustomInputProps {
  label: string;
  name: string;
  type: string;
  placeholder?: string;
}

const CustomInput = ({ label, ...props }: CustomInputProps) => {
  const [field, meta] = useField(props);

  return (
    <div className="form-control">
      <label htmlFor={props.name}>{label}</label>
      <input className="custom-input" {...field} {...props} />
      <div className="error-wrapper">
        {meta.touched && meta.error ? (
          <div className="error">{meta.error}</div>
        ) : null}
      </div>
    </div>
  );
};

interface CustomSelectProps {
  label: string;
  name: string;
  children: ReactNode;
}

const CustomSelect = ({ label, ...props }: CustomSelectProps) => {
  const [field, meta] = useField(props);

  return (
    <div className="form-control">
      <label htmlFor={props.name}>{label}</label>
      <select {...field} {...props} />
      <div className="error-wrapper">
        {meta.touched && meta.error ? (
          <div className="error">{meta.error}</div>
        ) : null}
      </div>
    </div>
  );
};

interface CustomChoiceGroupProps {
  id: string;
  label: string;
  name: string;
  type: string;
  options: { name?: string; value: string }[];
}
const CustomChoiceGroup = ({
  id,
  label,
  name,
  type,
  options,
}: CustomChoiceGroupProps) => {
  const [field, meta] = useField(name);

  return (
    <div className="form-control">
      <label className="options-heading" htmlFor={name} id={id}>
        {label}
      </label>
      <div role="group" aria-labelledby={id}>
        {options.map((option) => (
          <label className="options" htmlFor={name} key={option.value}>
            <input {...field} type={type} name={name} value={option.value} />
            {option.name || option.value}
          </label>
        ))}
      </div>
      <div className="error-wrapper">
        {meta.touched && meta.error ? (
          <div className="error">{meta.error}</div>
        ) : null}
      </div>
    </div>
  );
};

interface FormValues {
  firstName: string;
  lastName: string;
  country: string;
  gender: string;
  dob: string;
  languages: string[];
  address: string;
}

const validationSchema = Yup.object({
  firstName: Yup.string()
    .max(15, 'Must be 15 characters or less')
    .required('Required'),
  lastName: Yup.string()
    .max(20, 'Must be 20 characters or less')
    .required('Required'),
  country: Yup.string()
    .oneOf(['India', 'USA', 'Canada', 'other'], 'You must choose one.')
    .required('Required'),
  gender: Yup.string()
    .oneOf(['Male', 'Female', 'Other'], 'You must choose one.')
    .required('Required'),
  dob: Yup.date()
    .min('2000-01-01', 'Your date of birth should be after 1st Jan,2000 ')
    .required('Required'),
  languages: Yup.array()
    .of(Yup.string().oneOf(['English', 'Hindi', 'French', 'German']))
    .min(1, 'Choose atleast one.'),
  address: Yup.string()
    .min(10, 'Must be 10 character or less')
    .required('Required'),
});

interface OtherProps {
  message: string;
}

const radioOptions = [
  { value: 'Male' },
  { value: 'Female' },
  { value: 'Other' },
];

const checkboxOptions = [
  { value: 'English' },
  { value: 'Hindi' },
  { value: 'French' },
  { value: 'German' },
];

const InnerForm = (props: OtherProps & FormikProps<FormValues>) => {
  const { touched, errors, isSubmitting, message } = props;
  return (
    <Form id="registrationForm">
      <h1 className="form-head">{message}</h1>
      <CustomInput label="First Name" name="firstName" type="text" />

      <CustomInput label="Last Name" name="lastName" type="text" />

      <CustomInput label="Date of Birth" name="dob" type="date" />

      <CustomSelect label="Country" name="country">
        <option value="">Select a country</option>
        <option value="India">India</option>
        <option value="USA">USA</option>
        <option value="Canada">Canada</option>
        <option value="other">Other</option>
      </CustomSelect>

      <CustomChoiceGroup
        type="radio"
        id="genderGroup"
        label="Gender"
        name="gender"
        options={radioOptions}
      />

      <CustomChoiceGroup
        type="checkbox"
        id="languageGroup"
        label="Languages Known"
        name="languages"
        options={checkboxOptions}
      />

      <div className="form-control">
        <label htmlFor="address">Address</label>
        <Field as="textarea" name="address" />
        <div className="error-wrapper">
          {touched.address && errors.address && (
            <div className="error">{errors.address}</div>
          )}
        </div>
      </div>

      <button id="registrationBtn" type="submit" disabled={isSubmitting}>
        Submit
      </button>
    </Form>
  );
};

interface RegistrationFormProps {
  message: string;
}

const RegistrationForm = withFormik<RegistrationFormProps, FormValues>({
  mapPropsToValues: () => {
    return {
      firstName: '',
      lastName: '',
      country: '',
      gender: '',
      dob: '',
      languages: [],
      address: '',
    };
  },

  validationSchema: validationSchema,

  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      alert(JSON.stringify(values, null, 2));

      setSubmitting(false);
    }, 400);
  },
})(InnerForm);

export default RegistrationForm;
