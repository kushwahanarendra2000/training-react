import { Component } from "react";
import { CheckedBoxIcon, DeleteIcon, UncheckedBoxIcon } from "../SVGs/Icons";
import "./ToDo.css";

export default class ToDo extends Component {
  constructor(props) {
    super(props);
    const { data } = this.props;
    this.state = { edit: false, task: data.task };
  }

  handleEdit = () => {
    const { allfunctions, index } = this.props;
    if (this.state.task) {
      allfunctions.changeToDo(index, this.state.task);
      this.setState({ edit: false });
    }
  };

  render() {
    const { allfunctions, index, data } = this.props;
    const { toggleToDoStatus, deleteToDo } = allfunctions;
    return (
      <div className="to-do-item">
        <div className="done-button-wrapper">
          <button
            type="button"
            className="done-button"
            onClick={() => toggleToDoStatus(index)}
          >
            {data.complete ? <CheckedBoxIcon /> : <UncheckedBoxIcon />}
          </button>
        </div>
        <div className="to-do-edit">
          {this.state.edit ? (
            <div className="edit-task">
              <input
                className="task-input"
                type="text"
                value={this.state.task}
                placeholder="Add Task"
                onChange={(e) => this.setState({ task: e.target.value })}
              />
              <button
                className="edit-button"
                type="button"
                onClick={this.handleEdit}
              >
                Set
              </button>
            </div>
          ) : (
            <button
              className="edit-task-button"
              type="button"
              onClick={() => this.setState({ edit: true })}
            >
              <span className="to-do">{data.task}</span>
            </button>
          )}
        </div>
        <div className="delete-button-wrapper">
          <button
            type="button"
            className="delete-button"
            onClick={() => deleteToDo(index)}
          >
            <DeleteIcon />
          </button>
        </div>
      </div>
    );
  }
}
