import { Component } from "react";
import { PlusIcon } from "../SVGs/Icons";
import "./AddButton.css";

export default class AddButton extends Component {
  constructor(props) {
    super(props);
    this.state = { inputToDo: "", showInput: false };
  }

  handleClick = () => {
    if (!this.state.showInput) {
      this.setState({ showInput: true });
    } else {
      if (this.state.inputToDo) {
        this.props.addToDoRef.current(this.state.inputToDo);
        this.setState({ inputToDo: "", showInput: false });
      }
    }
  };

  render() {
    return (
      <div className="add-to-do-wrapper">
        {this.state.showInput ? (
          <div className="add-field">
            <input
              className="add-input"
              value={this.state.inputToDo}
              type="text"
              onChange={(e) => this.setState({ inputToDo: e.target.value })}
              placeholder="Add To-Do"
            />
          </div>
        ) : null}
        <div className="add-button-wrapper">
          <button
            className="add-button"
            type="button"
            onClick={this.handleClick}
          >
            <PlusIcon />
          </button>
        </div>
      </div>
    );
  }
}
