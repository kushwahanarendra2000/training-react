import { Component } from "react";
import ToDo from "../ToDo/ToDo";
import "./ToDoList.css";

export default class ToDoList extends Component {
  constructor(props) {
    super(props);
    this.state = { dataLoaded: false, toDoList: [] };
  }

  addToDo = (toDo) => {
    this.setState((prevState) => ({
      toDoList: [...prevState.toDoList, { task: toDo, complete: false }],
    }));
  };

  deleteToDo = (delIndex) => {
    this.setState((prevState) => ({
      toDoList: [
        ...prevState.toDoList.filter((element, index) => index !== delIndex),
      ],
    }));
  };

  toggleToDoStatus = (toggleIndex) => {
    this.setState((prevState) => ({
      toDoList: [
        ...prevState.toDoList.map((element, index) =>
          index === toggleIndex
            ? { ...element, complete: !element.complete }
            : element
        ),
      ],
    }));
  };

  changeToDo = (changeIndex, toDo) => {
    this.setState((prevState) => ({
      toDoList: [
        ...prevState.toDoList.map((element, index) =>
          index === changeIndex ? { ...element, task: toDo } : element
        ),
      ],
    }));
  };

  componentDidMount() {
    if (!this.state.dataLoaded) {
      this.setState({ dataLoaded: true });
      this.props.setAddToDoRef(this.addToDo);
      const storageToDoList = localStorage.getItem("to-do-list");

      if (storageToDoList) {
        const parsedJSON = JSON.parse(storageToDoList);
        this.setState({ toDoList: parsedJSON.data });
      }
    }
  }

  componentDidUpdate() {
    localStorage.setItem(
      "to-do-list",
      JSON.stringify({ data: this.state.toDoList })
    );
  }

  render() {
    return (
      <div className="to-do-list">
        {this.state.toDoList.map((data, index) => (
          <ToDo
            key={index}
            index={index}
            data={data}
            allfunctions={{
              toggleToDoStatus: this.toggleToDoStatus,
              deleteToDo: this.deleteToDo,
              changeToDo: this.changeToDo,
            }}
          />
        ))}
      </div>
    );
  }
}
