import { Component, createRef } from "react";
import "./App.css";
import AddButton from "./Components/AddButton/AddButton";
import ToDoList from "./Components/ToDoList/ToDoList";

const Header = () => (
  <header className="app-header">
    <h1>To-Do App</h1>
  </header>
);

export default class App extends Component {
  constructor(props) {
    super(props);
    this.addToDoRef = createRef();
  }

  setAddToDoRef = (func) => {
    this.addToDoRef.current = func;
  };

  render() {
    return (
      <div className="App">
        <Header />
        <section className="to-do-section">
          <ToDoList setAddToDoRef={this.setAddToDoRef} />
          <AddButton addToDoRef={this.addToDoRef} />
        </section>
      </div>
    );
  }
}
