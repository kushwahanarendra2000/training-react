import { useCallback, useRef } from 'react';
import './App.css';
import AddButton from './Components/AddButton/AddButton';
import { ToDoList, addToDo } from './Components/ToDoList/ToDoList';

function Header() {
  return (
    <header className="app-header">
      <h1>To-Do App</h1>
    </header>
  );
}

export default function App(): JSX.Element {
  const addToDoRef = useRef<addToDo | null>(null);

  const setAddToDoRef = useCallback((func: addToDo) => {
    addToDoRef.current = func;
  }, []);

  return (
    <div className="App">
      <Header />
      <section className="to-do-section">
        <ToDoList setAddToDoRef={setAddToDoRef} />
        <AddButton addToDoRef={addToDoRef} />
      </section>
    </div>
  );
}
