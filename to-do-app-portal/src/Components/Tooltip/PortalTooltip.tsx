import { ReactNode, useState } from 'react';
import Portal from '../Portal/Portal';
import './Tooltip.css';

enum TooltipDirection {
  'top',
  'right',
  'bottom',
  'left',
}

interface PortalTooltipProps {
  children: ReactNode;
  content: string | JSX.Element;
  delay?: number;
  direction?: TooltipDirection;
  coordinates: { top: number; left: number };
}

export default function PortalTooltip({
  children,
  delay,
  content,
  direction,
  coordinates,
}: PortalTooltipProps): JSX.Element {
  let timeout: ReturnType<typeof window.setTimeout>;
  const [active, setActive] = useState(false);

  const showTip = () => {
    timeout = setTimeout(() => {
      setActive(true);
    }, delay);
  };

  const hideTip = () => {
    clearInterval(timeout);
    setActive(false);
  };

  return (
    <div
      className="Tooltip-Wrapper"
      onMouseEnter={showTip}
      onMouseLeave={hideTip}
    >
      {children}
      <Portal>
        {active && (
          <div
            style={{ ...coordinates }}
            className={`Tooltip-Tip ${direction}`}
          >
            {content}
          </div>
        )}
      </Portal>
    </div>
  );
}

PortalTooltip.defaultProps = {
  delay: 400,
  direction: 'right',
};
