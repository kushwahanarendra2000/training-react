import { ReactNode, useState } from 'react';
import './Tooltip.css';

enum TooltipDirection {
  'top',
  'right',
  'bottom',
  'left',
}

interface TooltipProps {
  children: ReactNode;
  content: string | JSX.Element;
  delay?: number;
  direction?: TooltipDirection;
}

export default function Tooltip({
  children,
  delay,
  content,
  direction,
}: TooltipProps): JSX.Element {
  let timeout: ReturnType<typeof window.setTimeout>;
  const [active, setActive] = useState(false);

  const showTip = () => {
    timeout = setTimeout(() => {
      setActive(true);
    }, delay);
  };

  const hideTip = () => {
    clearInterval(timeout);
    setActive(false);
  };

  return (
    <div
      className="Tooltip-Wrapper"
      onMouseEnter={showTip}
      onMouseLeave={hideTip}
    >
      {children}
      {active && <div className={`Tooltip-Tip ${direction}`}>{content}</div>}
    </div>
  );
}

Tooltip.defaultProps = {
  delay: 400,
  direction: 'right',
};
