import { useState, useEffect, useCallback } from 'react';
import ToDo from '../ToDo/ToDo';
import './ToDoList.css';

export type addToDo = (toDo: string) => void;

type setAddToDoRef = (func: addToDo) => void;

type toDo = { task: string; complete: boolean; key: number };

interface ToDoListProps {
  setAddToDoRef: setAddToDoRef;
}

export function ToDoList({ setAddToDoRef }: ToDoListProps): JSX.Element {
  const [toDoList, setToDoList] = useState<toDo[]>([]);

  function addToDo(toDo: string): void {
    setToDoList((prevState) => [
      ...prevState,
      { task: toDo, complete: false, key: Math.floor(Math.random() * 10 ** 6) },
    ]);
  }

  const deleteToDo = useCallback((delIndex: number): void => {
    setToDoList((prevState) => [
      ...prevState.filter((element, index) => index !== delIndex),
    ]);
  }, []);

  const toggleToDoStatus = useCallback((toggleIndex: number): void => {
    setToDoList((prevState) => [
      ...prevState.map((element, index) =>
        index === toggleIndex
          ? { ...element, complete: !element.complete }
          : element,
      ),
    ]);
  }, []);

  const changeToDo = useCallback((changeIndex: number, toDo: string): void => {
    setToDoList((prevState) => [
      ...prevState.map((element, index) =>
        index === changeIndex ? { ...element, task: toDo } : element,
      ),
    ]);
  }, []);

  useEffect(() => {
    setAddToDoRef(addToDo);
    const storageToDoList = localStorage.getItem('to-do-list');
    if (storageToDoList) {
      const parsedJSON = JSON.parse(storageToDoList);
      setToDoList(parsedJSON.data);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem('to-do-list', JSON.stringify({ data: toDoList }));
  }, [toDoList]);

  return (
    <div className="to-do-list">
      {toDoList.map((data, index) => (
        <ToDo
          key={data.key}
          index={index}
          data={data}
          allfunctions={{
            toggleToDoStatus,
            deleteToDo,
            changeToDo,
          }}
        />
      ))}
    </div>
  );
}
