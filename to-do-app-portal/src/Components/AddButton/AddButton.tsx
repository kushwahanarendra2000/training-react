import { useState, MutableRefObject, useCallback, useRef } from 'react';
import { PlusIcon } from '../SVGs/Icons';
import WindowPortal from '../Portal/WindowPortal';
import Tooltip from '../Tooltip/Tooltip';
import './AddButton.css';
import PortalTooltip from '../Tooltip/PortalTooltip';

type addToDo = (toDo: string) => void;
type setFocus = () => void;

interface AddButtonProps {
  addToDoRef: MutableRefObject<addToDo | null>;
}

export default function AddButton({ addToDoRef }: AddButtonProps): JSX.Element {
  const [inputToDo, setInputToDo] = useState('');
  const [showInputWindow, setShowInputWindow] = useState(false);
  const focusPopUpRef = useRef<setFocus | null>(null);
  const buttonRef = useRef<HTMLButtonElement | null>(null);
  const [coordinates, setCoordinates] = useState({ left: 0, top: 0 });

  const openWindow = useCallback(() => {
    setShowInputWindow(true);
  }, []);

  const closeWindow = useCallback(() => {
    setShowInputWindow(false);
  }, []);

  const setfocusPopUpRef = useCallback((setFocus: setFocus) => {
    focusPopUpRef.current = setFocus;
  }, []);

  function openPopup() {
    if (showInputWindow && focusPopUpRef.current) {
      focusPopUpRef.current();
    } else {
      openWindow();
    }
  }

  function handleAdd() {
    if (addToDoRef.current !== null) {
      addToDoRef.current(inputToDo);
      setInputToDo('');
      closeWindow();
    }
  }

  const handleCoordinateChange = useCallback(() => {
    if (buttonRef.current) {
      const rect = buttonRef.current.getBoundingClientRect();
      setCoordinates({
        left: rect.x + rect.width + 10,
        top: rect.y + window.scrollY + rect.height / 2,
      });
    }
  }, []);

  return (
    <div>
      <div className="add-to-do-wrapper">
        {showInputWindow && (
          <WindowPortal
            title="Add To-Do"
            closeWindow={closeWindow}
            setfocusPopUpRef={setfocusPopUpRef}
          >
            <div className="add-to-do-window">
              <h2 className="add-to-do-window-head">
                Add new to do to the list.
              </h2>
              <div className="add-to-do-wrapper">
                <div className="add-field">
                  <textarea
                    className="add-input"
                    value={inputToDo}
                    onChange={(e) => setInputToDo(e.target.value)}
                    placeholder="Add To-Do"
                  />
                </div>
                <div className="add-button-wrapper">
                  <button
                    className="add-button"
                    type="button"
                    onClick={handleAdd}
                  >
                    <PlusIcon />
                  </button>
                </div>
              </div>
            </div>
          </WindowPortal>
        )}
        <div className="add-button-wrapper">
          <Tooltip
            content={
              <>
                Opens window portal to <br /> add to do.
              </>
            }
          >
            <button className="add-button" type="button" onClick={openPopup}>
              <PlusIcon />
            </button>
          </Tooltip>
        </div>
      </div>
      <div className="add-to-do-wrapper">
        <div className="add-button-wrapper">
          <PortalTooltip
            coordinates={coordinates}
            content={
              <>
                Opens window portal to <br /> add to do.
              </>
            }
          >
            <button
              ref={buttonRef}
              className="add-button"
              type="button"
              onClick={openPopup}
              onMouseEnter={handleCoordinateChange}
            >
              <PlusIcon />
            </button>
          </PortalTooltip>
        </div>
      </div>
    </div>
  );
}
