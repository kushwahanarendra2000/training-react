import { ReactNode, useEffect, useRef } from 'react';
import { createPortal } from 'react-dom';

interface PortalProps {
  children: ReactNode;
}

export default function Portal({ children }: PortalProps): JSX.Element {
  const externalRoot = useRef(document.getElementById('tooltip-root'));
  const containerElement = useRef(document.createElement('div'));

  useEffect(() => {
    if (externalRoot.current) {
      externalRoot.current.appendChild(containerElement.current);
    }
    return () => {
      if (externalRoot.current) {
        externalRoot.current.removeChild(containerElement.current);
      }
    };
  }, []);

  return createPortal(children, containerElement.current);
}
