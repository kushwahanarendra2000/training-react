import { ReactNode, useCallback, useEffect, useRef } from 'react';
import { createPortal } from 'react-dom';

interface WindowPortalProps {
  title: string;
  children: ReactNode;
  closeWindow: () => void;
  setfocusPopUpRef: (fn: () => void) => void;
}

function copyStyles(sourceDoc: Document, targetDoc: Document) {
  Array.from(sourceDoc.styleSheets).forEach((styleSheet) => {
    if (styleSheet.cssRules) {
      // true for inline styles
      const newStyleEl = sourceDoc.createElement('style');

      Array.from(styleSheet.cssRules).forEach((cssRule) => {
        newStyleEl.appendChild(sourceDoc.createTextNode(cssRule.cssText));
      });

      targetDoc.head.appendChild(newStyleEl);
    } else if (styleSheet.href) {
      // true for stylesheets loaded from a URL
      const newLinkEl = sourceDoc.createElement('link');

      newLinkEl.rel = 'stylesheet';
      newLinkEl.href = styleSheet.href;
      targetDoc.head.appendChild(newLinkEl);
    }
  });
}

export default function WindowPortal({
  title,
  children,
  closeWindow,
  setfocusPopUpRef,
}: WindowPortalProps): JSX.Element {
  const containerElement = useRef(document.createElement('div'));
  const externalWindow = useRef<Window | null>(null);

  const setFocus = useCallback(() => {
    if (externalWindow.current != null) {
      externalWindow.current.focus();
    }
  }, []);

  useEffect(() => {
    externalWindow.current = window.open(
      '',
      '',
      'width=800,height=400,left=560,top=300',
    );

    setfocusPopUpRef(setFocus);

    if (externalWindow.current !== null) {
      externalWindow.current.document.body.appendChild(
        containerElement.current,
      );
      externalWindow.current.document.title = title;
      copyStyles(document, externalWindow.current.document);
      externalWindow.current.addEventListener('beforeunload', closeWindow);
    }
    return () => {
      if (externalWindow.current !== null) {
        externalWindow.current.removeEventListener('beforeunload', closeWindow);
        externalWindow.current.close();
      }
    };
  }, []);

  return createPortal(children, containerElement.current);
}
